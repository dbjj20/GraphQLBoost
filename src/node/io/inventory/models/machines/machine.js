const { setupSequelize }= require('../../../lib/Generator')
const Sequelize = require('sequelize')

module.exports = function (config) {
  const sequelize = setupSequelize(config);

  return sequelize.define('Machine', {
    name: {
      type: Sequelize.STRING
    },
    model:{
      type: Sequelize.STRING
    },
    serial:{
      type: Sequelize.STRING
    },
    brand:{
      type: Sequelize.STRING
    },
    comments:{
      type: Sequelize.STRING
    },
    EmployeeId:{
      type: Sequelize.STRING,
      field: 'employee_id'
    },
    DeviceTypeId:{
      type: Sequelize.STRING,
      field: 'device_type_id'
    }
  },{
      tableName:'machines',
      timestamps: false
    }
  )
}
