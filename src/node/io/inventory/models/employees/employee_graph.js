const { graphqlObject } = require('../../../lib/Generator')

let fields = [
  {key: 'id', value: 'ID'},
  {key: 'name', value: 'Str'},
  {key: 'last_name', value: 'Str'},
  {key: 'DepartmentId', value: 'Int'},
  {key: 'PositionId', value: 'Int'}
];
// let fie = [
//   {key: 'Owners', value: OwnerType, assocType: 'hasMany'}
// ];
module.exports.EmployeeType = graphqlObject.simple('Employee',fields)
module.exports.EmployeeInput = graphqlObject.simpleInput('Employee',fields)

