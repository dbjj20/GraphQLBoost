const { graphqlObject } = require('../../../lib/Generator')

let fields = [
  {key: 'id', value: 'ID'},
  {key: 'name', value: 'Str'},
  {key: 'model', value: 'Str'},
  {key: 'brand', value: 'Str'},
  {key: 'serial', value: 'Str'},
  {key: 'comments', value: 'Str'},

  {key: 'EmployeeId', value: 'Int'},
  {key: 'DeviceTypeId', value: 'Int'}
];
// let fie = [
//   {key: 'Owners', value: OwnerType, assocType: 'hasMany'}
// ];

module.exports.MachineType = graphqlObject.simple('Machine',fields)
module.exports.MachineInput = graphqlObject.simpleInput('Machine',fields)

