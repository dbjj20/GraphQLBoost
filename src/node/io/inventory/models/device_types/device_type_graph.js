const { graphqlObject } = require('../../../lib/Generator')
const { McDeviceType } = require('../mc_devices/mc_device_graph')
const { MachineType } = require('../machines/machine_graph')

let fields = [
  {key: 'id', value: 'ID'},
  {key: 'name', value: 'Str'},
];

let sub = [
  {key: 'McDevices', value: McDeviceType, assocType: 'hasMany'},
  {key: 'Machines', value: MachineType, assocType: 'hasMany'},
];
module.exports.DeviceTypeType = graphqlObject.complex('DeviceType',fields, sub)
module.exports.DeviceTypeInput = graphqlObject.simpleInput('DeviceType',fields)
