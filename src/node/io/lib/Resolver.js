class Resolver {
  constructor(model) {
    this.model = model
  }

  async getById(id) {
    let result = await this.model.findOne({
      where: {id: id},
      include: [{
        all: true,
        nested: true
      }]
    })
    return result
  }

  async getAll(pages, limit) {
    let options = {
      include: [{
        all: true,
        nested: true
      }]
    }

    if (pages > 0 || null || undefined && limit > 1 || null || undefined) {
      let result = await this.model.findAll()
      return result

    } else {
      Object.assign(options, { offset: pages, limit: limit })
      let result = await this.model.findAll(options)
      return result
    }
  }

  async create(itm) {
    let result = await this.model.create(itm)
    return result
  }

  async update(id, itm) {
    let item = await this.model.getById(id)
    const itmCopy = JSON.parse(JSON.stringify(item))
    item.update(itm)
    return Promise.resolve(itmCopy)
  }

  async destroy(id) {
    const itm = await this.model.getById(id)
    const itmCopy = JSON.parse(JSON.stringify(itm))
    itm.destroy()
    return Promise.resolve(itmCopy)
  }
}
exports.Resolver = Resolver
