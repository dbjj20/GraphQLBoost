const { setupSequelize } = require('../../../lib/Generator')
const Sequelize = require('sequelize')

module.exports = function (config) {
  const sequelize = setupSequelize(config);

  return sequelize.define('Department', {
      name: {
        type: Sequelize.STRING
      }
    },{
      tableName:'departments',
      timestamps: false
    }
  )
}
