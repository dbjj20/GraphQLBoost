const setupEmployeeModel = require('./employees/employee')
const setupMachineModel = require('./machines/machine')
const setupDeviceTypeModel = require('./device_types/device_type')
const setupMcDeviceModel = require('./mc_devices/mc_device')
const setupDepartmentModel = require('./departments/department')
const setupPositionModel = require('./positions/position')

module.exports = function (config) {
  const Employee = setupEmployeeModel(config);
  const Machine = setupMachineModel(config);
  const DeviceType = setupDeviceTypeModel(config);
  const McDevice = setupMcDeviceModel(config);
  const Department = setupDepartmentModel(config);
  const Position = setupPositionModel(config);

  /* --- relations goes here --- */
  Position.hasMany(Employee);

  McDevice.belongsTo(DeviceType);
  McDevice.belongsTo(Machine);

  Machine.belongsTo(Employee);
  Machine.belongsTo(DeviceType);
  Machine.hasMany(McDevice);

  Employee.belongsTo(Department);
  Employee.belongsTo(Position);
  Employee.hasMany(Machine);

  DeviceType.hasMany(McDevice);
  DeviceType.hasMany(Machine);

  Department.hasMany(Employee);


  return {
    Employee,
    Machine,
    DeviceType,
    McDevice,
    Department,
    Position
  }
}
