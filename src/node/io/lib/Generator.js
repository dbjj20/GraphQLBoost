const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLBoolean,
  GraphQLID,
  GraphQLSchema,
  GraphQLInt,
  GraphQLFloat,
  GraphQLList,
  GraphQLInputObjectType
} = require('graphql')
const Sequelize = require('sequelize')
const { Resolver } = require('./Resolver')
let sequelize = null;

exports.BuildSchema = {
  queryAndMutation(collectionOfModels, collectionOfModelsInputType) {
    return  new GraphQLSchema({
      query: new GraphQLObjectType({
        name:'Query',
        fields: BuildQuery(collectionOfModels)
      }),
      mutation: new GraphQLObjectType({
        name:'Mutation',
        fields: BuildMutation(collectionOfModelsInputType)
      })
    })
  }
};

const BuildQuery = (model) => {
  let QUERY = {};
  Object.values(model).map( (k) => {
    let resolver = new Resolver(k.model);
    return Object.assign(QUERY,
      {
        [k.key]: {
          type: new GraphQLList(k.value),
          args: {
            id: { type: GraphQLInt },
            pages: { type: GraphQLInt },
            limit: { type: GraphQLInt },
          },
          resolve: (value, {id, limit, pages}, ) => {
            return ( () => {
              if (id) {
                return [resolver.getById(id)];
              } else {
                return resolver.getAll()
              }
            })();
          }
        },
      });
  });

  return QUERY
}

const Build = {
    fields(model) {
      let OBJ = {};
      Object.values(model).map((k) => {
        return Object.assign(OBJ,
          {[k.key]: {type: getFields(k.value)}
          });
      });
      return OBJ
    },

    subFields(model,subModels) {
      let SUB = {};
      subModels.map((d) => (
        Object.values(model).map((k) => {
          let arr = new Array(d.assocType);
          switch (arr.toString()) {
            case 'hasMany':
              return Object.assign(SUB,
                {
                  [k.key]: { type: getFields(k.value) },
                  [d.key]: { type: new GraphQLList(d.value) },
                });
            case 'hasOne':
              return Object.assign(SUB,
                {
                  [k.key]: { type: getFields(k.value) },
                  [d.key]: { type: d.value },
                });
            default:
              return null
          }
        })));
      return SUB
    },

    inputFields(model,subModels){
      let SUB = {};
      subModels.map((d) => (
        Object.values(model).map((k) => {
          return Object.assign(SUB,
            {
              [k.key]: { type: getFields(k.value) },
              [d.key]: { type: d.value },
            });
        })
      ));
      return SUB
    }
}
exports.graphqlObject = {
  simple(modelName, model){
    return  new GraphQLObjectType({
      name: `${modelName}`,
      fields: Build.fields(model)
    })
  },
  complex(modelName, model, subModels){
    return  new GraphQLObjectType({
      name: `${modelName}`,
      fields: Build.subFields(model, subModels)
    })
  },
  simpleInput(modelName, model){
    return  new GraphQLInputObjectType({
      name: `${modelName}InputType`,
      fields: Build.fields(model)
    })
  },
  complexInput(modelName, model, subModels,){
    return  new GraphQLInputObjectType({
      name: `${modelName}InputType`,
      fields: Build.inputFields(model, subModels)
    })
  }
}

function getFields(val) {
    if (val === undefined || null){
      console.error("you must define an object with key and value EX: {key:'id',value:'ID'} and so on, " + "but you got: " + val)
    }else{
      let arr = new Array(val);
      switch (arr.toString()){
        case 'Str':
          return GraphQLString;
        case 'Int':
          return GraphQLInt;
        case 'Bool':
          return GraphQLBoolean;
        case 'ID':
          return GraphQLID;
        case 'Double':
          return GraphQLFloat;
        default:
          return GraphQLString
      }
    }
  }
  /*---   USAGE   ---*/
  /*
   let ho = [
   {key:'id',value:"ID"},
   {key:'name',value:"Str"},
   {key:'number',value:"Int"},
   {key:'enable',value:"Bool"},
   ];
   let a = [
   {key:'id',value:"ID"},
   {key:'name',value:"Str"},
   {key:'number',value:"Int"},
   {key:'enable',value:"Bool", input: inputModel}
   ];

   let hello = GraphObj("Hello", ho);
   let house2 = GraphObj("house2", ho)

   let subs = [
   {key: "hello", value: hello},
   {key: "house2", value: house2}
   ];

   let house = GraphObj("House", a, subs); this generate all the required objects to getStared
   */

  /* ---- Query, Mutation and Schema Generator  ---- */

function BuildMutation(modelInputType) {
    let MUTATION = {};
    Object.values(modelInputType).map((k) => {
      let resolver = new Resolver(k.model)
      return Object.assign(MUTATION,
        {
          [`create${k.key}`]: {
            type:k.value,
            args: {
              input: {
                type: k.input
              }
            },
            resolve: (value, { input }) => {
              return resolver.create(input)
            }
          },
          [`edit${k.key}`]: {
            type: k.value,
            args: {
              id: { type: GraphQLID },
              input:{
                type: k.input
              }
            },
            resolve: (value, { id, input }) => {
              return resolver.update(id,input)
            }
          },
          [`delete${k.key}`]: {
            type: k.value,
            args: {
              id: { type: GraphQLID }
            },
            resolve: (value, { id }) => {
              return resolver.destroy(id)
            }
          }
        });
    });
    return MUTATION
  }

  /* ----  Generate Sequelize Obj  --- */

  // export function GetSequelizeModel(modelName, jsonFields) {
  //   //console.log(jsonFields);
  //   //console.log(modelName)
  //   let json = JSON.parse(jsonFields);
  //   return db.define(`${modelName}`,
  //     json
  //     ,{tableName:`${modelName}`, timestamps:false
  //   });
  // }

 exports.setupSequelize = function (config) {
    if (!sequelize) {
      return new Sequelize(config)
    }

    return sequelize
  }

  // export function generateSequelize(modelName,asd) {
  //   let jso = "";
  //   for (let a = 0; a < asd.length; a++){
  //     jso += '{'+`${asd[a]}`+'}'
  //   }
  //   return GetSequelizeModel(modelName, jso)
  // }
  /* -- Json Example -- */
  /*
    let arr = [
      '"id": {"type": "Sequelize.INTEGER","autoIncrement": true,"primaryKey": true,"field": "owner_id"},'+
      ' "last_name": "Sequelize.STRING",'+
      ' "name": "Sequelize.STRING"'
    ];
  */
