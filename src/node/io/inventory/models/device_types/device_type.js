const { setupSequelize } = require('../../../lib/Generator')
const Sequelize = require('sequelize')

module.exports = function (config) {
  const sequelize = setupSequelize(config);

  return sequelize.define('DeviceType', {
      name: {
        type: Sequelize.STRING
      }
    },{
      tableName:'device_types',
      timestamps: false
    }
  )
}
