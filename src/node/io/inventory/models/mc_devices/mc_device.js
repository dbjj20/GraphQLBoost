const { setupSequelize } = require('../../../lib/Generator')
const Sequelize = require('sequelize')

module.exports =  function (config) {
  const sequelize = setupSequelize(config);

  return sequelize.define('McDevice', {
      name: {
        type: Sequelize.STRING
      },
      model:{
        type: Sequelize.STRING
      },
      serial:{
        type: Sequelize.STRING
      },
      comment:{
        type: Sequelize.STRING
      },
      MachineId:{
        type: Sequelize.STRING,
        field:'machine_id'
      },
      DeviceTypeId:{
        type: Sequelize.STRING,
        field:'device_type_id'
      }
    },{
      tableName:'mc_devices',
      timestamps: false
    }
  )
}
