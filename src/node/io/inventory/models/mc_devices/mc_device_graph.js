const { graphqlObject } = require('../../../lib/Generator')

let fields = [
  {key: 'id', value: 'ID'},
  {key: 'name', value: 'Str'},
  {key: 'model', value: 'Str'},
  {key: 'serial', value: 'Str'},
  {key: 'comment', value: 'Str'},

  {key: 'MachineId', value: 'Int'},
  {key: 'DeviceTypeId', value: 'Int'}
]
// let fie = [
//   {key: 'Owners', value: OwnerType, assocType: 'hasMany'}
// ];

module.exports.McDeviceType = graphqlObject.simple('McDevice', fields)
module.exports.McDeviceInput = graphqlObject.simpleInput('McDevice', fields)

