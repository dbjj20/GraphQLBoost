const { setupSequelize } = require('../../../lib/Generator')
// const {Owner}= require('../owner/owner')
const Sequelize = require('sequelize')

module.exports = function (config) {
  const sequelize = setupSequelize(config);

  return sequelize.define('Employee', {
    name: {
      type: Sequelize.STRING
    },
    last_name:{
      type: Sequelize.STRING
    },
    DepartmentId:{
      type: Sequelize.INTEGER,
      field: 'department_id'
    },
    PositionId:{
      type: Sequelize.STRING,
      field:'position_id'
    }
  },{
      tableName:'employees',
      timestamps: false
    }
  )
}
