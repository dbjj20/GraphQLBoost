const { graphqlObject } = require('../../../lib/Generator')
const { EmployeeType } = require('../employees/employee_graph')

let fields = [
  {key: 'id', value: 'ID'},
  {key: 'name', value: 'Str'},
];

let subFields = [
  {key: 'Employees', value: EmployeeType, assocType: 'hasMany'},
];

module.exports.DepartmentType = graphqlObject.complex('Department',fields, subFields)
module.exports.DepartmentInput = graphqlObject.simpleInput('Department',fields)

