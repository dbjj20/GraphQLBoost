const { graphqlObject } = require('../../../lib/Generator')
const { EmployeeType } = require('../employees/employee_graph')

let fields = [
  {key: 'id', value: 'ID'},
  {key: 'name', value: 'Str'},
];

let subFields = [
  {key: 'Employees', value: EmployeeType, assocType: 'hasMany'},
];

module.exports.PositionType = graphqlObject.complex('Position',fields, subFields)
module.exports.PositionInput = graphqlObject.simpleInput('Position',fields)

