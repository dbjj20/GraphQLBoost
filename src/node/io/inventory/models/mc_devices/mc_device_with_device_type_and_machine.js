const { graphqlObject } = require('../../../lib/Generator')
const { MachineType } = require('../machines/machine_graph')
const { DeviceTypeType } = require('../device_types/device_type_graph')

let fields = [
  {key: 'id', value: 'ID'},
  {key: 'name', value: 'Str'},
  {key: 'model', value: 'Str'},
  {key: 'serial', value: 'Str'},
  {key: 'comment', value: 'Str'}
]

let fie = [
  {key: 'Machine', value: MachineType, assocType: 'hasOne'},
  {key: 'DeviceType', value: DeviceTypeType, assocType: 'hasOne'}
]

module.exports.McDeviceWithDeviceTypeAndMachineType = graphqlObject.complex(
    'McDeviceWithEmployeeAndMachine', fields, fie)

module.exports.McDeviceWithDeviceTypeAndMachineInput = graphqlObject.simpleInput(
    'McDeviceWithEmployeeAndMachine', fields)

