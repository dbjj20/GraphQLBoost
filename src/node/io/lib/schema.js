const { BuildSchema } = require('./Generator')
const { EmployeeInput, EmployeeType } = require('../inventory/models/employees/employee_graph')
const { MachineInput, MachineType } = require('../inventory/models/machines/machine_graph')
const { DeviceTypeInput, DeviceTypeType } = require('../inventory/models/device_types/device_type_graph')
const { McDeviceInput, McDeviceType} = require('../inventory/models/mc_devices/mc_device_graph')
const { DepartmentInput, DepartmentType} = require('../inventory/models/departments/depatment_graph')
const { PositionInput, PositionType} = require('../inventory/models/positions/position_graph')
const getModels = require('../inventory/models/all')
const {
  McDeviceWithDeviceTypeAndMachineType,
  McDeviceWithDeviceTypeAndMachineInput
} = require('../inventory/models/mc_devices/mc_device_with_device_type_and_machine')

let config = {
  "development": {
    "username": "postgres",
    "password": "",
    "database": "invmach",
    "host": "localhost",
    "port": 5432,
    "dialect": "postgres"
  }
};

const models = getModels(config.development);

let output = [
  {key: "employee", value: EmployeeType, model: models.Employee},
  {key: "machine", value: MachineType, model: models.Machine},
  {key: "device_type", value: DeviceTypeType, model: models.DeviceType},
  {key: "mc_device", value: McDeviceType, model: models.McDevice},
  {key: "department", value: DepartmentType, model: models.Department},
  {key: "position", value: PositionType, model: models.Position},
  {key: "mc_device_device_type_machine", value: McDeviceWithDeviceTypeAndMachineType
  , model: models.McDevice}
];

let input = [
  {key: "Employee", value: EmployeeType, input: EmployeeInput, model: models.Employee},
  {key: "Machine", value: MachineType, input: MachineInput, model: models.Machine},
  {key: "DeviceType", value: DeviceTypeType, input: DeviceTypeInput, model: models.DeviceType},
  {key: "McDevice", value: McDeviceType, input: McDeviceInput, model: models.McDevice},
  {key: "Department", value: DepartmentType, input: DepartmentInput, model: models.Department},
  {key: "Position", value: PositionType, input: PositionInput, model: models.Position},
  {key: "mc_device_device_type_machine", value: McDeviceWithDeviceTypeAndMachineType
  ,input: McDeviceWithDeviceTypeAndMachineInput, model: models.McDevice}
];

const schema = BuildSchema.queryAndMutation(output, input);

module.exports = schema
