const express = require('express')
const graphqlHTTP = require('express-graphql')
const schema = require('../lib/schema')
const cors = require('cors')
const http = require('http')

let app = express();

app.server = http.createServer(app);
app.options('/inventory', cors());
app.use(cors());

app.use('/inventory', graphqlHTTP({
  schema: schema,
  graphiql: true,
}));

app.set('port', (process.env.PORT || 8080));
app.listen(app.get('port'), function() {
  console.log('Node app is running on port', app.get('port'), '/inventory');
});
