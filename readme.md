###hi :)

###step 1
* `"npm install"` to install Dependencies and devDependencies

###step 2

* `"npm run dev"` to run development mode
* `"npm start"` to run production mode

###step 3 add sequelize models
* NOTE! `all of this steps are defined...`

* define sequelize model

  ```
    const Sequelize = require('sequelize')

    module.exports = function (config) {
      const sequelize = setupSequelize(config)

      return sequelize.define('Hello', {
        name: {
          type: Sequelize.STRING
        },
        last_name:{
          type: Sequelize.STRING
        }
      },{
          tableName:'hello',
          timestamps: false
        }
      )
    }
  ```

* then define a Object of relations

  ```
    const setupHelloModel = require('./hello/hello')
    const setupOwnerModel = reqiure('./owner/owner')

    module.exports = function (config) {
      const Hello = setupHelloModel(config);
      const Owner = setupOwnerModel(config);

      Hello.hasOne(Owner);
      Owner.belongsTo(Hello);

      return {
        Hello,
        Owner
      }
    }
  ```

this kind of setup a Sequelize model is independently and then you can control each of them
###step 4 let's make graphql objects
* make a normal javaScript object
  ```
    let fields = [
      {key: 'id', value: 'ID'},
      {key: 'name', value: 'Str'},
      {key: 'last_name', value: 'Str'}
    ]

    exports.HelloType = graphqlObject.simple('Hello',fields);
  ```
  
  graphqlObject its a generator of graphqlObjects that constructs a GraphQLObjectType

  key = `name of field`,
  value = `type of field such as Integer or String or Boolean or Double` but abbreviated

  if it has a sub model EX ` hellos has many owners or has one owner` you need to do this

  obviously, we need to import all the objects of graphql

  ```
    const { graphqlObject } = require('../../../lib/Generator')
    const { OwnerType } = require('../owner/owner_graph')

    let fields = [
      {key: 'id', value: 'ID'},
      {key: 'name', value: 'Str'},
      {key: 'last_name', value: 'Str'}
    ]
    let subFields = [
      {key: 'Owner', value: OwnerType, assocType: 'hasOne'}
    ]

    exports.HelloType = graphqlObject.complex('Hello',fields, subFields)
    exports.HelloInput = graphqlObject.simpleInput('Hello',fields)
  ```

  in this case we two types of field defined
  inputs types are for mutations types, mutations a query types are generated automatically

  assocType = `if you don't define it your relationship of graphql won't work hasMany or hasOne by now`

  in complex option you need to define a sub model but you need to create it before using it with `graphqlObject.simple()` method or complex, whatever you want

  * and then we need to define this to our schema
  configuration of the db and other things are in this file `/lib/schema.js`
  ```
    let config = {
      "development": {
        "username": "postgres",
        "password": "",
        "database": "glinpa",
        "host": "localhost",
        "port": 5432,
        "dialect": "postgres"
      }
    }

    const models = getModels(config.development)

    let output = [
      {key: "hello", value: HelloType, model: models.Hello},
      {key: "owner", value: OwnerType, model: models.Owner}
    ]

    let input = [
      {key: "Hello", value: HelloType, input: HelloInput, model: models.Owner},
      {key: "Owner", value: OwnerType, input: OwnerInput, model: models.Owner},
    ]
  ```
  we need to define the output type where `HelloType` its a GraphQLObjecType

  * and that's it all, it wil work now


*merge request rules*

* make a branch with your changes
* push your branch to proyect and make a new merge request
* then, wait ;)
