const { setupSequelize } = require('../../../lib/Generator')
const Sequelize = require('sequelize')

module.exports = function (config) {
  const sequelize = setupSequelize(config);

  return sequelize.define('Position', {
      name: {
        type: Sequelize.STRING
      }
    },{
      tableName:'positions',
      timestamps: false
    }
  )
}
